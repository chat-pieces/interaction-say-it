tmp = require 'tmp'
run = require '@aurium/run'
sayToPerson = null
sayToAnyone = null

verb = (euW, euRE, vcW, vcRE)->
    euRE ?= ///#{euW}///
    vcRE ?= ///#{vcW}///
    eu: { w:euW, re:euRE }, vc: { w:vcW, re:vcRE }
verbosPT = # vcRE match "você" and "tú" variations
  ir_pres    : verb 'vou',     null   , 'vai',    /vais?/
  ir_past    : verb 'fui',     null   , 'foi',    /foi|fostes?/
  ir_futu    : verb 'irei',    null   , 'irá',    /ir[aá]h?s?/
  ser_pres   : verb 'sou',     null   , 'é',      /é|eh|[ée]s/
  ser_past   : verb 'fui',     null   , 'foi',    /foi|f[oô]st[êe]s?/
  ser_futu   : verb 'serei',   null   , 'será',   /ser[áa]h?s?/
  ter_pres   : verb 'tenho',   null   , 'tem',    /tem|tens/
  ter_past   : verb 'tive',    null   , 'teve',   /teve|tiveste|tinha|tivera/
  ter_futu   : verb 'terei',   null   , 'terá',   /ter[aá]s?|terias?/
  estar_pres : verb 'estou',   null   , 'está',   /est[aá]s?|estejas?/
  estar_past : verb 'estive',  null   , 'esteve', /esteves?|estiveste|estivesses?|estavas?|estivera|estarias?/
  estar_futu : verb 'estarei', null   , 'estará', /estar[aá]s?|estivera?s?/

mkVerbPrimToTerc = (pess)->
    (match, b1, p, verb, b2)->
        for verbTense, variations of verbosPT
            if variations.eu.re.test verb
                return b1 + pess + ' ' + variations.vc.w + b2

mkVerbTercToPrim = (pess)->
    (match, b1, p, verb, b2)->
        for verbTense, variations of verbosPT
            if variations.vc.re.test verb
                return b1 + pess + ' ' + variations.eu.w + b2

module.exports = sayIt = (bot, update)->

    msg = update._act?.text or ''
    msgId = update._act?.message_id or 0
    chatId = update._act?.chat?.id or 0
    chatName = update._act?.chat?.username

    limit = '(^|\\s|[.,:;?!¿¡\'"]|$)'

    cmd = msg.match sayToPerson
    if cmd
        [origMsg, voice, person, textToSay] = cmd
        textToSay = person.replace(/@/g, ' ') + ', ' + textToSay
            .replace ///#{limit}(eu)\s+([^\s]+)#{limit}///g, mkVerbPrimToTerc update._act.from.username
            .replace ///#{limit}(eu)#{limit}///g, "$1#{update._act.from.username}$3"
            .replace ///#{limit}(voc[êe]|vc|t[úu])\s+([^\s]+)#{limit}///g, mkVerbTercToPrim 'eu'
            .replace ///#{limit}(voc[êe]|vc|t[úu])#{limit}///g, '$1eu$3'
            .replace ///#{limit}(el[eai])#{limit}///g, '$1você$3'
    else if cmd = msg.match sayToAnyone
        [origMsg, voice, textToSay] = cmd
            .replace ///#{limit}(eu)\s+([^\s]+)#{limit}///g, mkVerbPrimToTerc update._act.from.username
            .replace ///#{limit}(eu)#{limit}///g, "$1#{update._act.from.username}$3"
            .replace ///#{limit}(voc[êe]|vc|t[úu])\s+([^\s]+)#{limit}///g, mkVerbTercToPrim 'eu'
            .replace ///#{limit}(voc[êe]|vc|t[úu])#{limit}///g, '$1eu$3'

    voice = 'pt' unless voice

    if textToSay
        bot.logger.log 'SayIt:', textToSay, cmd
        tmp.file (err1, tempWavPath)-> tmp.file postfix: '.mp3', (err2, tempMp3Path)->
            err = err1 or err2
            at = 'at ' + if chatName then '@'+chatName else 'somewhere'
            if (err)
                bot.logger.error err
                bot.msgToAdm "Say-it fail #{at}: #{err.message}"
                bot.sendMessage 'Não consigo dizer isso.', chatId, reply_to_message_id: msgId
                return
            run 'espeak', '-v', voice, '-w', tempWavPath, textToSay
            .then ->
                run 'ffmpeg', '-y', '-i', tempWavPath, tempMp3Path, timeout: 20
            .then ->
                bot.sendVoice tempMp3Path, chatId
                bot.msgToAdm "Say-it #{at}: #{textToSay}"
            .catch (err)->
                bot.logErrorAndMsgAdm err, "Say-it fail #{at}, to msg \"#{textToSay}\""
                bot.sendMessage 'Não consigo dizer isso.', chatId, reply_to_message_id: msgId
            .then ->
                do tmp.setGracefulCleanup

sayIt.init = (bot)->
    if bot.username
        preRE = "^\\s*(?:@?(?:#{bot.name}|#{bot.username}|bot)[,\\s]*)?"
        sayToPerson = ///
            #{preRE}(?:diga|fale)\s+
            (?:em\s+([-_a-z0-9]+)\s+)?
            (?:para|pr[ao]|[àao]+)(?:\s+[ao])?\s+([^\s]+)
            (?:\s*:|\s+que\s+|\s+)(.*)
        ///ims
        sayToAnyone = ///
            #{preRE}(?:diga|fale)\s+
            (?:em\s+([-_a-z0-9]+)\s+)?
            (?:\s+que\s+|\s*:\s*|\s+)(.*)
        ///ims
    else
        setTimeout (-> sayIt.init bot), 200

